//
//  ViewController.swift
//  ClevertecIterations
//
//  Created by Oleg Kanatov on 11.02.22.
//

import UIKit

class ViewController: UIViewController {
    
    let myView = View()
    
    var photos = [String](repeating: ["Image1", "Image2", "Image3", "Image4", "Image5", "Image6", "Image7", "Image8", "Image9", "Image10"], count: 100)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func loadView() {
        view = myView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myView.tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.reuseId)
        myView.tableView.delegate = self
        myView.tableView.dataSource = self
    }
}

//-------------------------------------------------
// MARK: - UITableViewDelegate, UITableViewDataSource
//-------------------------------------------------

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.reuseId, for: indexPath) as? TableViewCell else { return UITableViewCell() }
        
        cell.iconImage.image = UIImage(named: "\(photos[indexPath.row])")
        cell.titleLabel.text = "Title\(indexPath.row)"
        cell.descriptionLabel.text = "Description \(indexPath.row)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as? TableViewCell
        
        let viewController = InfoViewController()
        viewController.infoView.iconImage.image = cell?.iconImage.image
        viewController.infoView.titleLabel.text = cell?.titleLabel.text
        viewController.infoView.descriptionLable.text = cell?.descriptionLabel.text
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            photos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let movedObject = photos.remove(at: sourceIndexPath.row)
        photos.insert(movedObject, at: destinationIndexPath.row)
    }
}

