//
//  InfoViewController.swift
//  ClevertecIterations
//
//  Created by Oleg Kanatov on 12.02.22.
//

import UIKit

class InfoViewController: UIViewController {
    
    let infoView = InfoView()
    
    override func loadView() {
        view = infoView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = false
    }
}
