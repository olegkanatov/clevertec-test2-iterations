//
//  InfoView.swift
//  ClevertecIterations
//
//  Created by Oleg Kanatov on 12.02.22.
//

import UIKit

class InfoView: UIView {
    
    lazy var iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Image1")
        return imageView
    }()
    
    private lazy var descriptionStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.spacing = 20
        return stack
    }()
    
    lazy var titleLabel = UILabel()
    lazy var descriptionLable = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        descriptionStack.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLable.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(iconImage)
        addSubview(descriptionStack)
        descriptionStack.addArrangedSubview(titleLabel)
        descriptionStack.addArrangedSubview(descriptionLable)
        
        guard let iconImageHeight = iconImage.image?.size.height else { return }
        guard let iconImageWidth = iconImage.image?.size.width else { return }
        
        NSLayoutConstraint.activate([
            
            iconImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            iconImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            iconImage.heightAnchor.constraint(equalToConstant: iconImageHeight * 2),
            iconImage.widthAnchor.constraint(equalToConstant: iconImageWidth * 2),
            
            descriptionStack.topAnchor.constraint(equalTo: iconImage.bottomAnchor, constant: 40),
            descriptionStack.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
    }
}
