//
//  TableViewCell.swift
//  ClevertecIterations
//
//  Created by Oleg Kanatov on 12.02.22.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    static let reuseId = "tableCell"
    
    lazy var iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        imageView.backgroundColor = .gray
        imageView.layer.cornerRadius = self.frame.size.height / 2
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var descriptionStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.spacing = 0
        return stack
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        descriptionStack.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(iconImage)
        addSubview(descriptionStack)
        descriptionStack.addArrangedSubview(titleLabel)
        descriptionStack.addArrangedSubview(descriptionLabel)
        
        NSLayoutConstraint.activate([
            
            iconImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            iconImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            iconImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),
            
            descriptionStack.leadingAnchor.constraint(equalTo: iconImage.trailingAnchor, constant: 10),
            descriptionStack.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            descriptionStack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5)
        ])
    }
}
